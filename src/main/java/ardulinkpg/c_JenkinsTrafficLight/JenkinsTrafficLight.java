package ardulinkpg.c_JenkinsTrafficLight;

import static ardulinkpg.jenkins.util.Color.BLUE;
import static ardulinkpg.jenkins.util.Color.RED;
import static ardulinkpg.jenkins.util.Color.YELLOW;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import org.zu.ardulink.Link;

import ardulinkpg.jenkins.util.Color;
import ardulinkpg.jenkins.util.OneLedOnlyOn;
import ardulinkpg.jenkins.util.PinAssignment;
import ardulinkpg.util.Iterators;

public class JenkinsTrafficLight {

	private final OneLedOnlyOn<Color> oneLedOnlyOn;

	public static void main(String[] args) throws IOException,
			InterruptedException {
		new JenkinsTrafficLight();
	}

	private Link link;

	public JenkinsTrafficLight() throws InterruptedException {
		this.link = Link.getDefaultInstance();
		this.link.connect("/dev/ttyUSB0", 9600);
		this.oneLedOnlyOn = new OneLedOnlyOn<Color>(this.link, PinAssignment
				.builder(Color.class).assign(BLUE, 10).assign(YELLOW, 9)
				.assign(RED, 7).build());

		while (true) {
			rotate(500, MILLISECONDS);
			rotate(100, MILLISECONDS);
			rotate(100, MILLISECONDS);
			rotate(50, MILLISECONDS);
			rotate(10, MILLISECONDS);
			rotate(1, SECONDS);
			for (Color color : Color.values()) {
				blink(color, 12, 250, MILLISECONDS);
			}
			for (Color color : Color.values()) {
				blink(color, 5, 750, MILLISECONDS);
			}
			for (Color color : Color.values()) {
				blink(color, 30, 100, MILLISECONDS);
			}
			for (Color color : Color.values()) {
				blink(color, 40, 50, MILLISECONDS);
			}
		}
	}

	private void blink(Color powerOn, int times, int t, TimeUnit tu)
			throws InterruptedException {
		System.out.println(String.format(
				"Blinking color %s %s times with a delay of %s %s", powerOn,
				times, t, tu));

		boolean state = true;
		for (int i = 0; i < times; i++) {
			if (state) {
				oneLedOnlyOn.powerOn(powerOn);
			} else {
				oneLedOnlyOn.powerOffAll();
			}
			state = !state;
			delay(t, tu);
		}
	}

	private void rotate(int t, TimeUnit tu) throws InterruptedException {
		System.out.println(String.format("Rotating with a delay of %s %s", t,
				tu));
		Iterator<Color> colors = Iterators.cycle(Color.values());
		for (int i = 0; i < Color.values().length * 3; i++) {
			Color color = colors.next();
			System.out.println(color);
			this.oneLedOnlyOn.powerOn(color);
			delay(t, tu);
		}
	}

	private static void delay(int t, TimeUnit tu) throws InterruptedException {
		tu.sleep(t);
	}

}

package ardulinkpg.jenkins.util;

import static org.zu.ardulink.protocol.IProtocol.HIGH;
import static org.zu.ardulink.protocol.IProtocol.LOW;

import org.zu.ardulink.Link;

public class OneLedOnlyOn<T extends Enum<?>> {

	private final Link link;
	private final PinAssignment<T> assignment;
	private final int[] pins;

	public OneLedOnlyOn(Link link, PinAssignment<T> assignment) {
		this.link = link;
		this.assignment = assignment;
		this.pins = assignment.getPins();
	}

	public void powerOffAll() {
		powerOn(null);
	}

	public void powerOn(T t) {
		int pinToPowerOn = this.assignment.get(t);
		for (int pin : pins) {
			this.link.sendPowerPinSwitch(pin, pin == pinToPowerOn ? HIGH : LOW);
		}
	}

}

package ardulinkpg.jenkins.util;

public class PinAssignment<T extends Enum<?>> {

	public static <T extends Enum<?>> PinMapBuilder<T> builder(Class<T> type) {
		return new PinMapBuilder<T>(type);
	}

	public static class PinMapBuilder<T extends Enum<?>> {

		private final int[] assignments;

		public PinMapBuilder(Class<T> type) {
			this.assignments = new int[type.getEnumConstants().length];
		}

		public PinMapBuilder<T> assign(T key, int pin) {
			this.assignments[key.ordinal()] = pin;
			return this;
		}

		public PinAssignment<T> build() {
			return new PinAssignment<T>(this.assignments);
		}
	}

	private final int[] map;

	public PinAssignment(int[] map) {
		this.map = map.clone();
	}

	public int get(T t) {
		return t == null ? -1 : map[t.ordinal()];
	}

	public int[] getPins() {
		return map.clone();
	}

}
package ardulinkpg.d_systraytimer;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ardulinkpg.d_systraytimer.StateBuilder.State.Blink;

public class StateBuilder {

	public static class States {

		private List<State> states;

		public States(StateBuilder stateBuilder) {
			states = sort(new ArrayList<State>(stateBuilder.states));
		}

		private List<State> sort(List<State> states) {
			Collections.sort(states, comparator());
			return states;
		}

		private Comparator<State> comparator() {
			return new Comparator<State>() {
				@Override
				public int compare(State s1, State s2) {
					return Integer.compare(s1.secs, s2.secs);
				}
			};
		}

		public State get(int secs) {
			State state = null;
			for (State next : states) {
				state = next;
				if (state.secs >= secs) {
					return state;
				}
			}
			return state;
		}

	}

	public static class State {

		public static enum Blink {
			ON, BLINK;
		}

		private final int secs;
		private final Color color;
		private final Blink blink;

		public State(int secs, Color color, Blink blink) {
			this.secs = secs;
			this.color = color;
			this.blink = blink;
		}

		public Color getColor() {
			return color;
		}

		public boolean isBlink() {
			return blink == Blink.BLINK;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((blink == null) ? 0 : blink.hashCode());
			result = prime * result + ((color == null) ? 0 : color.hashCode());
			result = prime * result + secs;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			State other = (State) obj;
			if (blink != other.blink)
				return false;
			if (color == null) {
				if (other.color != null)
					return false;
			} else if (!color.equals(other.color))
				return false;
			if (secs != other.secs)
				return false;
			return true;
		}

	}

	private final List<State> states = new ArrayList<State>();

	public static StateBuilder newInstance() {
		return new StateBuilder();
	}

	private StateBuilder() {
		super();
	}

	public StateBuilder initial(Color color) {
		return at(Integer.MAX_VALUE, color);
	}

	public StateBuilder at(int secs, Color color) {
		return at(secs, color, Blink.ON);
	}

	public StateBuilder at(int secs, Color color, Blink blink) {
		states.add(new State(secs, color, blink));
		return this;
	}

	public States build() {
		return new States(this);
	}

}

package ardulinkpg.d_systraytimer;

import static ardulinkpg.d_systraytimer.StateBuilder.State.Blink.BLINK;
import static ardulinkpg.jenkins.util.Color.BLUE;
import static ardulinkpg.jenkins.util.Color.RED;
import static ardulinkpg.jenkins.util.Color.YELLOW;
import static java.util.concurrent.TimeUnit.SECONDS;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.image.BufferedImage;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import javax.swing.SwingUtilities;

import org.zu.ardulink.Link;

import ardulinkpg.d_systraytimer.StateBuilder.State;
import ardulinkpg.d_systraytimer.StateBuilder.States;
import ardulinkpg.jenkins.util.OneLedOnlyOn;
import ardulinkpg.jenkins.util.PinAssignment;

public class SessionTimer {

	private static final States states = StateBuilder.newInstance()
			.initial(Color.GREEN).at(20, Color.YELLOW).at(10, Color.RED, BLINK)
			.at(3, Color.RED).build();

	private final OneLedOnlyOn<ardulinkpg.jenkins.util.Color> oneLedOnlyOn;

	private static final Map<Color, ardulinkpg.jenkins.util.Color> mapping = Collections
			.unmodifiableMap(mapping());

	private static Map<Color, ardulinkpg.jenkins.util.Color> mapping() {
		Map<Color, ardulinkpg.jenkins.util.Color> mapping = new HashMap<Color, ardulinkpg.jenkins.util.Color>();
		mapping.put(Color.GREEN, BLUE);
		mapping.put(Color.YELLOW, YELLOW);
		mapping.put(Color.RED, RED);
		return mapping;

	}

	private Link link;
	private boolean ledOnOff = true;

	private Image createImage(int secondsLeft, Color background) {
		BufferedImage image = new BufferedImage(24, 24,
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = image.createGraphics();
		if (background != null) {
			g.setColor(background);
			g.fillRect(0, 0, image.getWidth(), image.getHeight());
		}
		g.setColor(Color.BLACK);
		g.setFont(new Font(Font.DIALOG, Font.BOLD, 12));
		if (secondsLeft >= 0) {
			String text = String.valueOf(secondsLeft);
			FontMetrics fm = g.getFontMetrics();
			int x = (image.getWidth() - fm.stringWidth(text)) / 2;
			int y = (fm.getAscent() + (image.getHeight() - (fm.getAscent() + fm
					.getDescent())) / 2);
			g.drawString(text, x, y);
		}
		g.dispose();
		return image;
	}

	public static void main(String[] args) throws Exception {
		SwingUtilities.invokeAndWait(new Runnable() {
			@Override
			public void run() {
				new SessionTimer();
			}
		});
	}

	public SessionTimer() {
		this.link = Link.getDefaultInstance();
		this.link.connect("/dev/ttyUSB0", 9600);
		this.oneLedOnlyOn = new OneLedOnlyOn<ardulinkpg.jenkins.util.Color>(
				this.link, PinAssignment
						.builder(ardulinkpg.jenkins.util.Color.class)
						.assign(BLUE, 10).assign(YELLOW, 9).assign(RED, 7)
						.build());

		// see http://forum.arduino.cc/index.php?topic=243300.5;wap2
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}

		SystemTray tray = SystemTray.getSystemTray();
		final TrayIcon trayIcon = new TrayIcon(createImage(-1, Color.WHITE));
		try {
			tray.add(trayIcon);
		} catch (AWTException e) {
			throw new RuntimeException(e);
		}
		new Timer().schedule(new TimerTask() {

			private int secsLeft = 60;

			@Override
			public void run() {
				secsLeft--;
				if (secsLeft >= 0) {
					State state = states.get(secsLeft);
					ledOnOff = state.isBlink() ? !ledOnOff : true;
					Color background = state.getColor();
					trayIcon.setImage(createImage(secsLeft,
							ledOnOff ? background : null));

					if (ledOnOff) {
						oneLedOnlyOn.powerOn(mapped(background));
					} else {
						oneLedOnlyOn.powerOffAll();
					}
				} else {
					stopTimer();
				}
			}

			private ardulinkpg.jenkins.util.Color mapped(Color awtColor) {
				return mapping.get(awtColor);
			}

			private void stopTimer() {
				cancel();
			}
		}, 0, SECONDS.toMillis(1));
	}
}

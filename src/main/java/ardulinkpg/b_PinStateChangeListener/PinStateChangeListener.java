package ardulinkpg.b_PinStateChangeListener;

import static org.zu.ardulink.protocol.IProtocol.HIGH;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.zu.ardulink.Link;
import org.zu.ardulink.event.DigitalReadChangeEvent;
import org.zu.ardulink.event.DigitalReadChangeListener;

public class PinStateChangeListener {

	public static void main(String[] args) throws InterruptedException,
			IOException {
		Link link = Link.getDefaultInstance();
		link.connect("/dev/ttyUSB0", 9600);

		// see http://forum.arduino.cc/index.php?topic=243300.5;wap2
		TimeUnit.SECONDS.sleep(2);

		try {
			for (int i = 2; i <= 12; i++) {
				link.addDigitalReadChangeListener(digitalReadChangeListener(i));
			}
			wait4ever();
		} finally {
			link.disconnect();
		}
	}

	private static void wait4ever() throws InterruptedException {
		Object o = new Object();
		synchronized (o) {
			o.wait();
		}
	}

	private static DigitalReadChangeListener digitalReadChangeListener(
			final int pin) {
		return new DigitalReadChangeListener() {
			@Override
			public void stateChanged(DigitalReadChangeEvent event) {
				System.out.println("pin " + event.getPin()
						+ " value changed value to "
						+ (event.getValue() == HIGH ? "on" : "off"));
			}

			@Override
			public int getPinListening() {
				return pin;
			}
		};
	}

}

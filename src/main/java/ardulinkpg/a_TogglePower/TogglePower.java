package ardulinkpg.a_TogglePower;

import static org.zu.ardulink.protocol.IProtocol.HIGH;
import static org.zu.ardulink.protocol.IProtocol.LOW;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.zu.ardulink.Link;

public class TogglePower {

	private static final int pin = 13;

	public static void main(String[] args) throws IOException {
		Link link = Link.getDefaultInstance();
		link.connect("/dev/ttyUSB0", 9600);

		// see http://forum.arduino.cc/index.php?topic=243300.5;wap2
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}

		boolean state = false;
		while (true) {
			link.sendPowerPinSwitch(pin, state ? HIGH : LOW);
			pause();
			state = !state;
		}
	}

	private static void pause() throws IOException {
		System.in.read();
	}

}

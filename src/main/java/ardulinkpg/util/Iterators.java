package ardulinkpg.util;

import java.util.Iterator;

public class Iterators {

	@SafeVarargs
	public static <T> Iterator<T> cycle(final T... values) {
		return new Iterator<T>() {

			private int pos;

			@Override
			public boolean hasNext() {
				return true;
			}

			@Override
			public T next() {
				T next = values[pos];
				this.pos = (this.pos + 1) % values.length;
				return next;
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}

		};
	}

}
